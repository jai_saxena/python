def get_cs():
    cs=input()
    return cs
    
def cs_to_dict(cs):
    cs2=[i.split('=') for i in cs.split(';')]
    new_dict={j[0]:j[1] for j in cs2}
    return new_dict
    
def dict_to_cs(d):
    cs=""
    for i in d:
        cs+= i + '=' + d[i] + ';'
    return cs
    
def main():
    cs=get_cs()
    d=cs_to_dict(cs)
    print(d)
    cs=dict_to_cs(d)
    print(cs)
    
main()