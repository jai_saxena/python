def get_cs():
	cs=input()
	return cs
	
def cs_to_lot(cs):
	lot=[tuple(i.split("=")) for i in cs.split(';')]
	return lot

def main():
	cs=get_cs()
	lot=cs_to_lot(cs)
	print(f"{lot}")
	
main()