def get_cs():
	cs=input()
	return cs
	
def cs_to_lot(cs):
	lot=[tuple(i.split("=")) for i in  cs.split(";")]
	return lot
    
def lot_to_cs(lot):
	cs=['='.join(i) for i in lot]
	return ';'.join(cs)
    
    
def main():
	cs=get_cs()
	lot=cs_to_lot(cs)
	print(f'{lot}')
	cs=lot_to_cs(lot)
	print(f"{cs}")
	
	
main()