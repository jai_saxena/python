class menu:
    def __init__(self):
        self.food={}


    def add(self,name,x):
        self.food[name]=x

    def show(self):
        for i,j in self.food.items():
            print(i,j)
    
    def get_price(self,x):
        if x not in self.food:
            return None
        else:
            return self.food[x]
            

m=menu()
m.add("idli",10)
m.add("vada",20)
m.add("dosa",0)
m.show()
a=input("enter the food item: ")
if (m.get_price(a)==None):
    print("item not in menu")
else:
    print(f"the price is {m.get_price(a)}")